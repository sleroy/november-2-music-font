For [Finale](https://klemm-music.de/makemusic/finale/index.php), Sibelius & LilyPond. By [Robert Piéchaud](https://www.finalemusic.com/blog/robert-piechaud-november-2-0/).

![](https://forum.ircam.fr/media/uploads/software/November%202/main31-1117x500.png)

## The Beautiful Music Font ##

November has been praised for years by musicians, publishers and engravers as one of the finest and most vivid fonts ever designed for music notation software.

For use in programs such as [Finale](https://klemm-music.de/makemusic/finale/index.php), Dorico, Sibelius or LilyPond, November 2 now includes an extraordinary large variety of symbols, from usual shapes such as noteheads, clefs and rests to rarer characters like microtonal accidentals, early ornaments and clefs, or special instrument techniques, ranging from the Renaissance to today’s avant-garde music, from solo to large orchestral scores.

![](https://forum.ircam.fr/media/uploads/software/November%202/strip1.png)

November 2 has been thouroughly beta-tested by selected international composers and musicians among the IRCAM forum community and beyond ; notably, a subset of November 2 characters has been chosen by the developers of the [bach project](https://forum.ircam.fr/projects/detail/bach-computer-aided-composition-in-max/) as their new official font.

Moreover, November 2 is the first commercial font to comply with [SMuFL](https://www.smufl.org)– Standard Music Font Layout -, the future of music font standard initiated by Steinberg Media Technologies and based on [Unicode](https://en.wikipedia.org/wiki/Unicode).

![](https://forum.ircam.fr/media/uploads/software/November%202/strip2.png)

The November 2 package includes advanced support for [Finale](https://klemm-music.de/makemusic/finale/index.php), Dorico, Sibelius & LilyPond, and is delivered with useful extras, rich metadata and a comprehensive 200-page [documentation](https://www.klemm-music.de/notation/november2/en/presentation/November_2.0_Presentation_Engl.pdf).

Based on the principle that each detail means as much as the whole, crafted with ultimate care, November 2 is a font of unequalled coherence. While in tune with the most recent technologies, its inspiration comes from the art of traditional music engraving.

![](https://forum.ircam.fr/media/uploads/software/November%202/strip3.png)

Whichever period of music you deal with, November 2 will inspire you with its extended symbol palette, and will make your scores look beautiful and vibrant, as never before.

## Highlights of new November 2.2 version ##

- More than a hundred new symbols (in total: +1300)
- An optimal compatibility with Dorico
- A new doc quicklaunch plug-in for Finale

## Technical Specifications ##

- **Character Map:** November 2 owns over 1.300 music characters and is the first commercially-released font to comply with the new standard [SMuFL](https://www.smufl.org).

- **Font files:** beside the new November2 font, the November 2 package also includes legacy November & NovemberExtra fonts. All are wrapped in universal Open Type Format (OTF) with PostScript outlines.

- **Component files:** For a best user experience, November 2 ships with many specific components for Finale, Sibelius. Dorico and LilyPond: libraries, house styles and plug-in, templates, snippets etc.

- November 2 is available as [boxed version](https://www.klemm-music.de/shop/artikel.php?pd_id=KLEHNOR200E) from our online shop or as [direct download](https://klemm-music.de/notation/november2/en/download.php).

- November 2 is available as a Single Licence package allowing three computer installations (for individuals), as well as 10-user or 25-user Site Licences (for schools or organizations).

## Compatibility ##

- **Finale** 2012 or superior: full compatibility, for Mac & Windows. Easy conversion from Finale fonts (Maestro, Petrucci, etc.) or from November 1.0.

- Older versions of Finale can also be used with November & NovemberExtra.

- **Dorico:** compatibility thanks to native SMuFL design.

- **Sibelius** 6, 7 or superior [please note]: November & NovemberExtra as main fonts, and November2-specific characters can be typed as text. Mac only.

- **Lilypond** 2.9.15 or superior: excellent compatibility thanks to special .ly "snippets”. Mac, Linux & Windows.

- Potential use with other music notation programs.

- Installers for Mac & Windows, installation script on Linux.

> - [Official site](https://klemm-music.de/notation/november2/en/index.php)‘s
> - [**Blog:** Robert Piéchaud and November 2.0](https://www.finalemusic.com/blog/robert-piechaud-november-2-0/)
> - [Notation Examples](https://klemm-music.de/notation/november2/examples/November-2-2-Examples.English.pdf)
